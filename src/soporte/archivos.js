const fs = require('fs');
let directorio_win = '';
//let directorio_win = process.env.APPDATA + '\\KTec';
//const directorio_linux = '/tmp/KTec';

function crear_dir()
{
  fs.mkdir(directorio_win, function(err)
  {
    if (err)
    {
      if (err.code == 'EEXIST'){}
      else console.log(err);
    }
    else {};
  });
}

function escribir_archivos(items)
{
  crear_dir();

  const archivo1 = directorio_win + '\\actualizar.cmd';
  const archivo2 = directorio_win + '\\desinstalar_apps.cmd';
  const archivo3 = directorio_win + '\\instalar_apps.cmd';
  const archivo4 = directorio_win + '\\instalar_choco.cmd';
  const archivo5 = directorio_win + '\\packages.config';
  const archivo6 = directorio_win + '\\instalador_base.cmd';

  let paquetes = '<?xml version="1.0" encoding="utf-8"?>\n';
  paquetes = paquetes + '<packages>\n';

  fs.writeFile(archivo1, 'choco upgrade all -y', function (err) {
    if (err) {
      console.log(err);
    }
  });

  fs.writeFile(archivo2, 'choco uninstall "%appdata%\\KTec\\packages.config" -y', function (err)
  {
    if (err) {console.log(err);}
  });

  fs.writeFile(archivo3, 'choco install "%appdata%\\KTec\\packages.config" -y --allow-empty-checksums --ignore-checksums', function (err)
  {
    if (err) {console.log(err);}
  });

  fs.writeFile(archivo4, "@\"%SystemRoot%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command \"iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))\" && SET \"PATH=%PATH%;%ALLUSERSPROFILE%\\chocolatey\\bin", function (err)
  {
    if (err) {console.log(err);}
  });

  for (let i = 0, len = items.length; i < len; i++)
  {
    paquetes = paquetes + "\t" + '<package id="' + items[i] + '" />\n';
  }

  paquetes = paquetes + '</packages>\n';

  fs.writeFile(archivo5, paquetes, function (err)
  {
    if (err) {console.log(err);}
  });

  let base = '@echo off\n' + '\n' + '\n' + 'CLS\n' + 'ECHO.\n' +
    'ECHO =============================\n' + 'ECHO Instalador Base KTec\n' +
    'ECHO =============================\n' + '\n' + 'ECHO.\n';
  if (!process.env.ChocolateyInstall) {
    base = base +
      'ECHO =============================\n' + 'ECHO "Instalando Chocolatey ..."\n' +
      'ECHO =============================\n' + 'CALL "%appdata%\\KTec\\instalar_choco.cmd"\n' +
      '\n' + 'ECHO.\n';
  }
  base = base +
    'ECHO =============================\n' + 'ECHO "Instalando Aplicaciones ..."\n' +
    'ECHO =============================\n' + 'CALL "%appdata%\\KTec\\instalar_apps.cmd"\n' +
    '\n' + 'ECHO.\n' +
    'ECHO =============================\n' + 'ECHO "Programando Actualizaciones ..."\n' +
    'ECHO =============================\n' +
    'schtasks /create /RU SYSTEM /tn "Actualizar" /tr "%appdata%\\KTec\\actualizar.cmd" /sc monthly /F' +
    '\n' + '\n' + 'ECHO.\n' +
    'ECHO =============================\n' + 'ECHO "Finalizado."\n' +
    'ECHO =============================\n' + 'ECHO.\n' + 'ECHO.\n' + 'Pause\n' + '\n' + 'return 5' + ' endlocal';


  fs.writeFile(archivo6, base, function (err)
  {
    if (err) {console.log(err);}
  });

  return archivo6;
}

export function ejecutar(items)
{
  const arch = escribir_archivos(items);
  return arch;
}

//module.exports = ejecutar;
export function Apps(camino){
  const data = fs.readFileSync(camino+"/conf/programas.json");
  const apps = JSON.parse(data);
  return apps;
};  

export function setDir(dir)
{
  directorio_win = dir + '\\KTec';
}

export function leer_settings()
{  
  try {
    const data = fs.readFileSync(directorio_win + '\\settings.json');
    const apps = JSON.parse(data);
    return apps;
  } catch (error) {
    console.log(error);
    return false;
  }  
}

export function guardar_settings(ajustes)
{
  crear_dir();
  fs.writeFileSync(directorio_win + '\\settings.json', JSON.stringify(ajustes));
}