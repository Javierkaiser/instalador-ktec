import { app, protocol, BrowserWindow, ipcMain, shell } from 'electron';
import {
  createProtocol,
  installVueDevtools
} from 'vue-cli-plugin-electron-builder/lib';
import path from 'path';
import { ejecutar, setDir } from "./soporte/archivos";
import { execSync } from 'child_process';

const log = require('electron-log');
const elevator = require('elevator');
const isDevelopment = process.env.NODE_ENV !== 'production';
const os = require('os');
const fs = require('fs');

const dir = app.getPath('appData');
setDir(dir);
let win;
app.allowRendererProcessReuse = true;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{ scheme: 'app', privileges: { secure: true, standard: true } }])

function createWindow() {
  win = new BrowserWindow({
    width: 1000,
    height: 700,
    title: "KTec - Instalador Base",
    autoHideMenuBar: true,
    frame: false,
    show: false,
    icon: path.join(__static, 'icon.png'),
    webPreferences: { nodeIntegration: true }
  });

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  win.maximize();
  win.once('ready-to-show', () => {
    win.show();
  });

  win.on('closed', () => {
    win = null
  })

  win.webContents.send('Dir', true);

  win.webContents.on('new-window', (e, url) => {
    e.preventDefault();
    shell.openExternal(url);
  });
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

app.on('ready', async () => {
  /* if (isDevelopment && !process.env.IS_TEST) {
    try {
       await installVueDevtools()
    } catch (e) {
       console.error('Vue Devtools failed to install:', e.toString())
    }

  } */
  createWindow()
})



if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}

ipcMain.on('Montado', (e, _d) => {
  e.sender.send('Dir', dir);
});

ipcMain.on('EnvioActivos', async (event, arg) => {
  const arch = ejecutar(arg);
  let status = true;
  let vc_redist = "";

  //Activar en Windows
  try {
    if (os.arch() == "x64") {
      vc_redist = ".\\resources\\bin\\VC_redist.x64.exe";
    } else {
      vc_redist = ".\\resources\\bin\\VC_redist.x86.exe";
    }


    if (!fs.existsSync(`${process.env.SystemRoot}\\System32\\msvcr120.dll`)) {
      try {
        execSync(vc_redist);
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log(`Los archivos parecen estar`);
    }

    elevator.execute(arch, {
      waitForTermination: true
    }, (error, stdout, stderror) => {
      if (error) {
        console.error('Failed!');
        log.warn(error);
        status = false;
      }
      event.sender.send('Listo', status);
    });
  } catch (error) {
    log.warn(error);
    event.sender.send('Listo', status);
  }

});

ipcMain.on('Minimizo', (event, arg) => {
  win.minimize();
});
ipcMain.on('Maximizo', (event, arg) => {
  if (win.isMaximized()) {
    win.unmaximize();
  }
  else win.maximize();
});
ipcMain.on('Cierro', (event, arg) => {
  app.quit();
});
