# KTec - Instalador Base

## Descripción
Paquete de instalación de las aplicaciones básicas y necesarias  para cualquier instalación de Windows.

El paquete automáticamente descargara e instalara las versiones actuales del software, y hará comprobaciones mensuales de nuevas actualizaciones, y en caso de haberlas, las instalara sin molestar al usuario.

Uso libre, y gratuito.

Libre de virus.

Todo es mezcla entre software libre y freewares.

## Requerimientos:

Windows Vista, 7, 8, 10.

(No Xp)

Obligatorio acceso a Internet.

## Listado del Software incluido:
### Anti Virus:

    Avast
    Avira
    AVG
    Bit Defender
    Panda

### Edición:

    GIMP
    Paint.net
    Krita
    Inkscape

### Extras:

    Flash player plugin
    Flash player activex
    Silverlight
    Java
    DotNet3.5
    DotNet4.5
    DirectX

### Programación:

    Visual Studio Code
    Atom
    Nodejs
    Python
    Rust
    XAMPP
    Fira Code Font

### Herramientas:

    Microsoft Windows Terminal
    Infrarecorder
    wincdemu
    Teamviewer
    ChocolateyGUI
    7-Zip
    Driver Pack Online

### Internet:

    Firefox
    Google Chrome
    Skype
    Ares
    Thunderbird
    Zoom

### Mantenimiento:

    Bleachbit
    Ccleaner
    Speccy
    Revo.Uninstaller

### Multimedia:

    K-lite codec pack full
    Aimp
    Vlc
    Spotify

### Oficina:

    Libreoffice
    Foxit Reader