module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        "appId": "ktec.instalador.base",
        "productName": "KTec - Instalador Base",
        "files": [
          "**/*"
        ],
        "extraFiles": [
          "public/conf/*",
          {
            "from": "bin/",
            "to": "resources/bin/",
            "filter": ["**/*"]
          }
        ],
        "directories": {
          "buildResources": "build",
          "output": "dist"
        },
        "win": {
          "icon": "build/icon.ico",
          "target": "nsis",
          "asar": true
        },
        "nsis": {
          "oneClick": false,
          "perMachine": true,
          "allowElevation": true,
          "license": "build/LICENSE",
          "createDesktopShortcut": false,
          "language": "3082"
        }
      }
    }
  }
}